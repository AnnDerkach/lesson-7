import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class Interpreter{
	
	public static void main(String[] args){
		System.out.println("Command >");
		String command;
		try{
			do{
				Scanner in=new Scanner(System.in);
				command=in.nextLine();
				String result=CommandLine.typeCommand(CommandLine.splitLine(command),command.split(" "));
				switch(result){
					case "variables":
						Variables.substitution(CommandLine.splitLine(command));
						break;
					case "commands":
						CommandLine.defineCommand(command.split(" "),0);
						break;
					case "operators":
						Operators.defineOperator(command.split(" "));
						break;
					default:
						System.err.println("Wrong command");
				}
			}while(!command.equals("exit"));
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}

interface ExecuteCommand{
	public String execute(String[] s);
}



abstract class CommandLine{
	public static String defineCommand(String [] s, int pos){
		try{
			ExecuteCommand ex=null;
				switch (Commands.validate(s,pos)){
					case "cd":
						ex=new CdCommand();
						break;
					case "ls":
						ex=new LsCommand();
						break;
					case "pwd": 
						LsCommand ls=new LsCommand();
						LsCommand.PwdCommand pwd=ls.new PwdCommand();
						ex=pwd;
						break;
					case "du":
						ex=new DuCommand(); 
						break;
					case "df":
						ex=new DfCommand(); 
						break;
					case "head":
						ex=new HeadTailCommand(); 
						break;
					case "tail":
						ex=new HeadTailCommand(); 
						break;
					case "echo":
						DfCommand df=new DfCommand();
						DfCommand.EchoCommand echo=df.new EchoCommand();
						ex=echo;
						break;
					case "exit":
						System.exit(0);	
					default:
						throw new Exception("No such command");	
				}
				if(ex!=null){
					return ex.execute(s);
				}			
		}
		catch(Exception e){
			System.err.println(e);
		}
		return "null";
	}
	
	public static List<Character> splitLine(String s){
        List<Character> chars=new ArrayList<Character>();
        for(char c:s.toCharArray())
        	chars.add(c); 
		return chars;
	}
	
	public static String typeCommand(List<Character> list, String[] s){
		String[] op={"|",">",">>","&&"};
		
		try{
			List<Commands> cmd=Arrays.asList(Commands.values());	
			int flag=0;
			for(int k=0;k<s.length;k++)
				for(int n=0;n<op.length;n++)
					if(String.valueOf(s[k]).equals(op[n])){
						flag=2;
						Operators.operator=op[n];
						break;
					}		
			if(flag==2)
				return "operators";		
			for(int j=0;j<cmd.size();j++)
				if(String.valueOf(cmd.get(j)).equals(s[0])){
					flag=3;
					break;
				}
			if(flag==3)
				return "commands";
			else{
				for(int i=0;i<list.size();i++)
					if(list.get(i).equals('='))
						return "variables";
			}
		} catch (Exception e) {
			e.printStackTrace();	
			
		}
		return "null";
		
	}
}

class CdCommand implements ExecuteCommand{
	public static String currentPath="D:\\Java";

	@Override
	public String execute(String[] s) {
		try{
			if (new File(currentPath+"\\"+s[1]).exists()){
				this.currentPath=currentPath+"\\"+s[1];
			}
			else if(new File(s[1]).exists()){
				this.currentPath=s[1];
			}	
			else throw new FileNotFoundException("No such directory");
				return currentPath;
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
			
		}
		return null;
	}
}

class LsCommand implements ExecuteCommand{
	@Override
	public String execute(String[] s) {
		String res="";
		try{
			File[] files=new File(CdCommand.currentPath).listFiles();
	        if (files!=null) 
	            for (File f : files) {
	            	res+=f.getName()+"\n";
	                System.out.println(f.getName());
	            }
			return res;
		}
		catch (Exception e) {
			e.printStackTrace();
			
		}
		return "null";
	}
	
	class PwdCommand extends LsCommand implements ExecuteCommand {
		@Override
		public String execute(String[] s) {
			System.out.println(CdCommand.currentPath);
			return CdCommand.currentPath;
		}
	}
}


class DuCommand implements ExecuteCommand {
	@Override
	public String execute(String[] s) {
		long len=folderSize(new File(CdCommand.currentPath));
		System.out.println(len);
		return String.valueOf(len);
	}
	
	public long folderSize(File directory) {
	    long length=0;
	    for (File file : directory.listFiles()) {
	        if (file.isFile())
	            length+=file.length();
	        else
	            length+=folderSize(file);
	    }
	    return length;
	}
}

class DfCommand implements ExecuteCommand {
	@Override
	public String execute(String[] s) {
		File[] files=new File(CdCommand.currentPath).listFiles();
		String res="";
	    if (files != null) 
	        for (File f : files) {
	        	res+=f.getName()+" "+f.length()+"\n";
	            System.out.println(f.getName()+" "+f.length());
	        }
		return res;
	}
	
	public class EchoCommand implements ExecuteCommand{
		@Override
		public String execute(String[] s) {
			for(int i=1;i<s.length;i++)
				if(s[i].startsWith(Variables.vall))
					s[i]=Variables.var;
			String str=OperatorCommands.substringLine(s);
			String result=str.substring(5,str.length()-1);
			System.out.println(result);
			return result;
		}
				
	}
	public EchoCommand createEcho() { return new EchoCommand(); } 
}

class HeadTailCommand implements ExecuteCommand {
	public String fileName;
	public String cmdType;
	
	@Override
	public String execute(String[] s) {
		try{
			String res="";
			fileName=s[1];
			cmdType=s[0];
			int i=0;
			Scanner in = new Scanner(new File(CdCommand.currentPath+"\\"+fileName));
			List<String> list=new ArrayList<String>();
			if(cmdType.equals("head")){
				while (in.hasNextLine()&&i<10){
					res+=in.nextLine()+"\n";
					System.out.println(in.nextLine());
					i++;
				}
			}	
			else if(cmdType.equals("tail")){
				while (in.hasNextLine())
					list.add(in.nextLine());
				for(int j=list.size()-10; j<list.size();j++){
					System.out.println(list.get(j));
					res+=list.get(j)+"\n";
				}
			}
			return res;
		}	
		catch(Exception e){
			e.printStackTrace();
			
		}
		return "null";
	}
}

abstract class Variables {
	public static String var;
	public static String vall;
	
	public static String substitution(List<Character> list){
		vall=String.valueOf(list.get(0));
		var=String.valueOf(list.get(2));
		return var;
	}
}

enum Commands{
	cd, ls, pwd, df, du, head, tail, grep, exit, echo;
	
	public static String validate(String[] cmd, int pos) throws Exception {
		switch (cmd[pos]) {
			case "cd":
				return Commands.cd.toString();
			case "ls":
				return Commands.ls.toString();
			case "pwd":
				return Commands.pwd.toString();
			case "df":
				return Commands.df.toString();
			case "du":
				return Commands.du.toString();
			case "head":
				return Commands.head.toString();
			case "tail":
				return Commands.tail.toString();
			case "grep":
				return Commands.grep.toString();
			case "exit":
				return Commands.exit.toString();
			case "echo":
				return Commands.echo.toString();
			default:
				throw new Exception("No such command");
		}
	}
}	

abstract class Operators{
	public static String operator;
	
	public static boolean defineOperator(String [] s){
		try{
			ExecuteCommand ex=null;
				switch (operator){
					case "|":
						OperatorCommands opCmd=new OperatorCommands();
						OperatorCommands.TypeOne tp=opCmd.new TypeOne();
						ex=tp;
						break;
					case ">":
						OperatorCommands opCmd2=new OperatorCommands();
						OperatorCommands.TypeTwo tp2=opCmd2.new TypeTwo();
						ex=tp2;
					case ">>":
						OperatorCommands opCmd3=new OperatorCommands();
						OperatorCommands.TypeTwo tp3=opCmd3.new TypeTwo();
						ex=tp3;
						break;
					case "&&": 
						OperatorCommands opCmd1=new OperatorCommands();
						OperatorCommands.TypeOne tp1=opCmd1.new TypeOne();
						ex=tp1;
						break;
					default:
						throw new Exception("No such operator");	
				}
				if(ex!=null){
					ex.execute(s);
				}	
				return true;	
		}
		catch(Exception e){
			System.err.println(e);
		}
		return true;
	}
}

class OperatorCommands {
	public int cnt=1+Operators.operator.length(); 
	
	public static String substringLine(String[] s){
		StringBuilder sb=new StringBuilder();
		for (String str:s)
		   sb.append(str+" ");
		return sb.toString();	
	}
	
	public class TypeOne implements ExecuteCommand{
		@Override
		public String execute(String[] s) {
			try{
				String str=substringLine(s);
				String startCmd=str.substring(0,str.indexOf(Operators.operator)-1);
				String endCmd=str.substring(str.indexOf(Operators.operator)+cnt,str.length()-1);		
				if(Operators.operator.equals("|")){
					CommandLine.defineCommand(startCmd.split(" "),0);
					System.out.println(" ");
					CommandLine.defineCommand(endCmd.split(" "),0);
				}
				else if(Operators.operator.equals("&&")){
					if(!CommandLine.defineCommand(startCmd.split(" "),0).equals("null")){
						System.out.println(" ");
						CommandLine.defineCommand(endCmd.split(" "),0);
					}
				}
				return "";
			}
			catch(Exception e){
				e.printStackTrace();
				
			}
			return null;
		}
		
	}
	
	public class TypeTwo implements ExecuteCommand{
		@Override
		public String execute(String[] s) {
			FileWriter out=null;
			try{
				String str=substringLine(s);
				String command=str.substring(0,str.indexOf(Operators.operator)-1);
				String fileName=str.substring(str.indexOf(Operators.operator)+cnt,str.length()-1);
				File file=new File(CdCommand.currentPath+"\\"+fileName);
				if(!file.exists())
			            file.createNewFile();
				String res=CommandLine.defineCommand(command.split(" "),0);
				if(Operators.operator.equals(">")){
					out=new FileWriter(file, false);
					for(String string : res.split("\n")){
						out.write(string+System.lineSeparator());
					}
				}
				else if(Operators.operator.equals(">>")){
					for(String string : res.split("\n")){
						out.write(string+System.lineSeparator());
					}
				}
				return "";
			}
			catch(IOException e){
				e.printStackTrace();	
			}
			finally{
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return null;
		}
	}
}
	




